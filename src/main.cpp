#include <Arduino.h>
#include <SevSegShift.h>

#define NUM_OF_DIGITS 4

#define SHIFT_PIN_DS   2
#define SHIFT_PIN_STCP 3
#define SHIFT_PIN_SHCP 4

/* Problematic PINs --> do NOT use PWM PINs with DigitalWrite!!!
Disables confgiured timers*/
void change_TIMER() {
  /* changed in Arduino.h & variants/pins_arduino.h from const uint8_t to uint8_t to change it
  and prevent accidental Timer TurnOff during digitalWrite */
  for(int i=0; i<25; i++) {
    digital_pin_to_timer[i] = NOT_ON_TIMER;
  }
}

// DIGIT PINS = specifying one of the 4 digits
uint8_t digit_pins[] = {1+8, 4+8, 5+8, 7+8}; // These are the PINS of the ** Arduino **
// representing the actual Segement PINs
                      //  A  B  C  D  E  F  G  DP
uint8_t segment_pins[] = {2+8, 6+8, 4, 6, 7, 3+8, 3, 5} ; // these are the PINs of the ** Shift register **

uint8_t rgb_pins[3][3]={{A3, A2, A1},{A7, A6, A0},{9,8,7}};
uint8_t buttons[2][3] = {{1,2,3},{4,5,6}};

SevSegShift sevsegshift(
                  SHIFT_PIN_DS, 
                  SHIFT_PIN_SHCP, 
                  SHIFT_PIN_STCP
                );

ISR(TCB0_INT_vect) {
  // switch port pins between ALT & DEFAULT
  PORTMUX_TCBROUTEA = PORTMUX_TCBROUTEA ^ PORTMUX_TCB0_bm;
  TCB0_INTFLAGS |= TCB_CAPT_bm;
}

void setLEDWhite(int idLED){
  digitalWrite(rgb_pins[idLED][0], HIGH);
  digitalWrite(rgb_pins[idLED][1], HIGH);
  digitalWrite(rgb_pins[idLED][2], HIGH);
}
void setLEDRed(int idLED){
  digitalWrite(rgb_pins[idLED][0], HIGH);
  digitalWrite(rgb_pins[idLED][1], LOW);
  digitalWrite(rgb_pins[idLED][2], LOW);
}
void setLEDGreen(int idLED){
  digitalWrite(rgb_pins[idLED][0], LOW);
  digitalWrite(rgb_pins[idLED][1], HIGH);
  digitalWrite(rgb_pins[idLED][2], LOW);
}
void setLEDBlue(int idLED){
  digitalWrite(rgb_pins[idLED][0], LOW);
  digitalWrite(rgb_pins[idLED][1], LOW);
  digitalWrite(rgb_pins[idLED][2], HIGH);
}

void setLEDOff(int idLED){
  digitalWrite(rgb_pins[idLED][0], LOW);
  digitalWrite(rgb_pins[idLED][1], LOW);
  digitalWrite(rgb_pins[idLED][2], LOW);
}

void setup() {
  Serial.begin(9600);
  for (int i = 0; i<25; i++) {
    Serial.println(digital_pin_to_timer[i], DEC);
  }
  Serial.println("--");
  change_TIMER();
  for (int i = 0; i<25; i++) {
    Serial.println(digital_pin_to_timer[i], DEC);
  }
  // prepare sevsegshift
  sevsegshift.begin(
    COMMON_CATHODE,
    NUM_OF_DIGITS,
    digit_pins,
    segment_pins
  );

  TCA0_SPLIT_CTRLA = 0; //disable TCA
  TCA0_SPLIT_CTRLD = TCA_SPLIT_SPLITM_bm;

  TCA0_SPLIT_CTRLB = 
    TCA_SPLIT_HCMP0EN_bm |
    TCA_SPLIT_HCMP1EN_bm |
    TCA_SPLIT_HCMP2EN_bm |
    TCA_SPLIT_LCMP0EN_bm |
    TCA_SPLIT_LCMP1EN_bm |
    TCA_SPLIT_LCMP2EN_bm;

  TCA0_SPLIT_CTRLC =
    TCA_SPLIT_HCMP0OV_bm |
    TCA_SPLIT_HCMP1OV_bm |
    TCA_SPLIT_HCMP2OV_bm |
    TCA_SPLIT_LCMP0OV_bm |
    TCA_SPLIT_LCMP1OV_bm |
    TCA_SPLIT_LCMP2OV_bm;

  // frequency
  TCA0_SPLIT_HPER = 0xFF;
  TCA0_SPLIT_LPER = 0xFF;

  // dutycycle
  TCA0_SPLIT_HCMP0 = 0x0; // LED3 Green
  TCA0_SPLIT_HCMP1 = 0x0; // LED1 Green
  TCA0_SPLIT_HCMP2 = 0x0; // LED1 RED!?!?!? WO5/PD5 --> interfere with digital/TurnoffPWM on digital-Write...
  TCA0_SPLIT_LCMP0 = 0x0; // LED2 Green
  TCA0_SPLIT_LCMP1 = 0x0; // LED2 Blue
  TCA0_SPLIT_LCMP2 = 0x0; // LED3 Red

  // set PINS for OUTPUT
  PORTD_DIRSET |= PIN0_bm | PIN1_bm | PIN2_bm | PIN3_bm | PIN4_bm | PIN5_bm;

  // use the PORTD outputs for all TCAs (A0-A3/A6/A7)
  PORTMUX_TCAROUTEA = PORTMUX_TCA0_enum::PORTMUX_TCA0_PORTD_gc;

  TCA0_SPLIT_CTRLA = 
  TCA_SPLIT_CLKSEL_enum::TCA_SPLIT_CLKSEL_DIV64_gc | 
  TCA_SPLIT_ENABLE_bm;

  // prepare TCB-timers
  TCB0_CTRLA = 0;
  TCB1_CTRLA = 0;

  TCB0_CTRLB = TCB_CNTMODE_enum::TCB_CNTMODE_PWM8_gc |
    TCB_CCMPEN_bm;
  TCB1_CTRLB = TCB_CNTMODE_enum::TCB_CNTMODE_PWM8_gc |
    TCB_CCMPEN_bm;

  //frequency
  TCB0_CCMPL = 0x7F; // shared duty cycle
  TCB1_CCMPL = 0xFF;
  //duty_cycle
  TCB0_CCMPH = 0x0; // LED3 Blue | LED2 Red
  TCB1_CCMPH = 0x7F; // LED1 Blue

  PORTMUX_TCBROUTEA = 0; // default route (A4/A5) + alternate for TCB0=D6
  PORTA_DIRSET |= PIN2_bm | PIN3_bm;
  PORTF_DIRSET |= PIN5_bm; // for shared duty cycle 

  TCB0_CTRLA = TCB_CLKSEL_enum::TCB_CLKSEL_CLKTCA_gc | 
    TCB_ENABLE_bm;
  TCB1_CTRLA = TCB_CLKSEL_enum::TCB_CLKSEL_CLKTCA_gc | 
    TCB_ENABLE_bm;

  //enable ISR TCB0 for changing VALUES for BLUE & GREEN
  TCB0_INTCTRL |= TCB_CAPT_bm;

/*
  // prepare RGB pins
  for (int i=0;i<3;i++) {
    for(int j=0;j<3;j++) {
      pinMode(rgb_pins[i][j], OUTPUT);
    }
    setLEDRed(i);
    delay(1000);
    setLEDGreen(i);
    delay(1000);
    setLEDBlue(i);
    delay(1000);
    setLEDWhite(i);
    delay(1000);
    setLEDOff(i);
    delay(1000);
  }
  */
}

/****************************/
/* loop copied from example */
/****************************/
void loop() {
  static unsigned long timer = millis();
  static int deciSeconds = 0;

  if (millis() - timer >= 100) {
    timer += 100;
    deciSeconds++; // 100 milliSeconds is equal to 1 deciSecond

    if (deciSeconds == 10000) { // Reset to 0 after counting for 1000 seconds.
      deciSeconds=0;
    }
    sevsegshift.setNumber(deciSeconds, 1);
  }
  sevsegshift.refreshDisplay(); // Must run repeatedly
}
